
Flatpak packaging for Pd-extended.  To build:

    git submodule update --init
    flatpak-builder builddir --install-deps-from=flathub --user \
        --repo=pd-extended-flatpak --force-clean info.puredata.Pd-extended.yaml
